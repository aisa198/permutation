﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Permutation
{
    public class Program
    {
        static void Main()
        {
            Console.WriteLine(IsPermutation(Console.ReadLine(), Console.ReadLine()));
            Console.ReadKey();
        }

        public static string IsPermutation(string numbers1, string numbers2)
        {
            var listOfNumbers1 = numbers1.Split(' ').Select(x => Convert.ToInt32(x, new CultureInfo("pl-PL"))).ToList();
            var listOfNumbers2 = numbers2.Split(' ').Select(x => Convert.ToInt32(x, new CultureInfo("pl-PL"))).ToList();

            if (listOfNumbers1.Count == listOfNumbers2.Count)
            {
                foreach(var item in listOfNumbers1)
                {
                    listOfNumbers2.Remove(item);
                }
                return listOfNumbers2.Count == 0 ? "YES" : "NO";
            }
            return "NO";
        }
    }
}
