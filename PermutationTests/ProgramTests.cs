﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Permutation.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void IsPermutationTest_permutation_YES()
        {
            //arrange
            var numbers1 = "1 2 3 4 5 6 7 8 9 10 11";
            var numbers2 = "1 2 3 4 5 6 7 8 9 10 11";
            var numbers3 = "1 2 5 3 7 0 7 3 5 2 1";
            var numbers4 = "7 3 1 2 5 0 5 2 1 3 7";
            //act
            var result1 = Program.IsPermutation(numbers1, numbers2);
            var result2 = Program.IsPermutation(numbers3, numbers4);
            //assert
            Assert.AreEqual("YES", result1);
            Assert.AreEqual("YES", result2);
        }

        [TestMethod()]
        public void IsPermutationTest_notPermutation_NO()
        {
            //arrange
            var numbers1 = "1 2 3 4 5 6 7 8 9 10 0";
            var numbers2 = "1 2 3 4 5 6 7 8 9 10 11";
            var numbers3 = "1 2 3 4 5 6 7 8 9 10 11";
            var numbers4 = "6 5 4 3 2 0 11 10 9 8 7";
            //act
            var result1 = Program.IsPermutation(numbers1, numbers2);
            var result2 = Program.IsPermutation(numbers3, numbers4);
            //assert
            Assert.AreEqual("NO", result1);
            Assert.AreEqual("NO", result2);
        }

        [TestMethod()]
        public void IsPermutationTest_wrongNumberOfElements_NO()
        {
            //arrange
            var numbers1 = "1 2 3 4 5 6 7 8 9 10 11";
            var numbers2 = "1 2 3 4 5 6 7 8 9 10";
            //act
            var result = Program.IsPermutation(numbers1, numbers2);
            //assert
            Assert.AreEqual("NO", result);
        }
    }
}